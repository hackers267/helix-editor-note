# 快捷键

helix中使用的快捷键：

| 快捷键 | 作用 |
|----|-----|
|Alt-O|快捷键选择html标签内的内容|

## 正常模式

### 移动

|Key              |Description                                 |Command                   |
|-----------------|--------------------------------------------|--------------------------|
|h, Left          |	Move left	                                 |move_char_left            |
|j, Down          |	Move down	                                 |move_visual_line_down     |
|k, Up            |	Move up	                                   |move_visual_line_up       |
|l, Right         |	Move right	                               |move_char_right           |
|w                |	Move next word start	                     |move_next_word_start      |
|b                |	Move previous word start	                 |move_prev_word_start      |
|e                |	Move next word end	                       |move_next_word_end        |
|W                |	Move next WORD start	                     |move_next_long_word_start |
|B                |	Move previous WORD start	                 |move_prev_long_word_start |
|E                |	Move next WORD end	                       |move_next_long_word_end   |
|t                |	Find 'till next char	                     |find_till_char            |
|f                |	Find next char	                           |find_next_char            |
|T                |	Find 'till previous char	                 |till_prev_char            |
|F                |	Find previous char	                       |find_prev_char            |
|G                |	Go to line number <n>	                     |goto_line                 |
|Alt-.            |	Repeat last motion (f, t, m, [ or ])	     |repeat_last_motion        |
|Home             |Move to the start of the line	             |goto_line_start           |
|End              |Move to the end of the line	               |goto_line_end             |
|Ctrl-b, PageUp   |Move page up	                               |page_up                   |
|Ctrl-f, PageDown	|Move page down                              |page_down                 |
|Ctrl-u           |	Move cursor and page half page up	         |page_cursor_half_up       |
|Ctrl-d           |	Move cursor and page half page down	       |page_cursor_half_down     |
|Ctrl-i           |	Jump forward on the jumplist	             |jump_forward              |
|Ctrl-o           |	Jump backward on the jumplist	             |jump_backward             |
|Ctrl-s           |	Save the current selection to the jumplist |save_selection            |

### 修改

|快捷键 | 描述                                                                  |命令                     |
|-------|-----------------------------------------------------------------------|-------------------------|
|r      |	使用字符替换所选内容	                                                |replace                  |
|R      |	使用寄存器内容替换所选内容	                                          |replace_with_yanked      |
|~      |	转换所选内容的大小写	                                                |switch_case              |
|`      |	所选内容转为小写	                                                    |switch_to_lowercase      |
|Alt-`  |	所选内容转为大写	                                                    |switch_to_uppercase      |
|i      |	在选区前插入	                                                        |insert_mode              |
|a      |	在选区后插入(追加)	                                                  |append_mode              |
|I      |	在一行开头插入	                                                      |insert_at_line_start     |
|A      |	在一行末尾插入	                                                      |insert_at_line_end       |
|o      |	在选区后添加一行并进入插入模式	                                      |open_below               |
|O      |	在选区前添加一行并进入插入模式	                                      |open_above               |
|.      |	重复上一次插入                                                        |	N/A                     |
|u      |	撤销修改	                                                            |undo                     |
|U      |	重复修改	                                                            |redo                     |
|Alt-u  |	在历史中向后移动	                                                    |earlier                  |
|Alt-U  |	在历史中向前移动	                                                    |later                    |
|y      |	复制选择	                                                            |yank                     |
|p      |	在选区后粘贴	                                                        |paste_after              |
|P      |	在选区前粘贴	                                                        |paste_before             |
|" <reg>|	选择一个寄存器用于复制和粘贴	                                        |select_register          |
|>      |	添加缩进	                                                            |indent                   |
|<      |	减少缩进	                                                            |unindent                 |
|=      |	格式化选区 (currently nonfunctional/disabled) (LSP)	                  |format_selections        |
|d      |	删除选区内容	                                                        |delete_selection         |
|Alt-d  |	删除选区内容，但内容不进入寄存器	                                    |delete_selection_noyank  |
|c      |	修改选区内容 (删除内容后进入插入模式)	                                |change_selection         |
|Alt-c  |	修改内容 (删除内容后进入插入模式，内容不进入寄存器)	                  |change_selection_noyank  |
|Ctrl-a |	递增光标下的数值	                                                    |increment                |
|Ctrl-x |	递减光标下的数值	                                                    |decrement                |
|Q      |	记录/停止宏到选定的寄存器中 (experimental)	                          |record_macro             |
|q      |	播放指定选定寄存器中的宏内容 (experimental)	                          |replay_macro             |

### Shell

|Key  |Description                                                                        |Command                                                            |
|---  |-----------                                                                        |-------                                                            |
|     \|                                                                                  |	Pipe each selection through shell command,<br> replacing with output	|shell_pipe      |
|Alt- \|                                                                                  |	Pipe each selection into shell command,<br> ignoring output	          |shell_pipe_to   |
|!    |	Run shell command,<br> inserting output before each selection	                        |shell_insert_output                                                |
|Alt-!|	Run shell command,<br> appending output after each selection	                        |shell_append_output                                                |
|$    |	Pipe each selection into shell command,<br> keep selections where command returned 0	|shell_keep_pipe                                                    |

### 选择

|快捷键          |描述                                                            |命令                              |
|----------------|----------------------------------------------------------------|----------------------------------|
|s               |	在选区内选择符合指定正则表达式的内容	                        |select_regex                      |
|S               |	将选区按指定的正则表达式拆分为多个子选区	                    |split_selection                   |
|Alt-s           |	将选区按行拆分为多个子选区	                                  |split_selection_on_newline        |
|Alt-minus       |	合同选区	                                                    |merge_selections                  |
|Alt-_           |	合并连续选区	                                                |merge_consecutive_selections      |
|&               |	在列上对齐选区	                                              |align_selections                  |
|_               |	Trim whitespace from the selection	                          |trim_selections                   |
|;               |	Collapse selection onto a single cursor	                      |collapse_selection                |
|Alt-;           |	Flip selection cursor and anchor	                            |flip_selections                   |
|Alt-:           |	Ensures the selection is in forward direction	                |ensure_selections_forward         |
|,               |	只保留主选区	                                                |keep_primary_selection            |
|Alt-,           |	移除主选区	                                                  |remove_primary_selection          |
|C               |	Copy selection onto the next line (Add cursor below)	        |copy_selection_on_next_line       |
|Alt-C           |	Copy selection onto the previous line (Add cursor above)	    |copy_selection_on_prev_line       |
|(               |	主选区向后移动	                                              |rotate_selections_backward        |
|)               |	主选区向前移动	                                              |rotate_selections_forward         |
|Alt-(           |	选区内容依次向后移动	                                        |rotate_selection_contents_backward|
|Alt-)           |	选区内容依次向前移动	                                        |rotate_selection_contents_forward |
|%               |	文件全选	                                                    |select_all                        |
|x               |	选择当前行，如何当前行已选择，选择下一行	                    |extend_line_below                 |
|X               |	扩展选择到整行 (line-wise selection)	                        |extend_to_line_bounds             |
|Alt-x           |	缩小选择到整行? (line-wise selection)	                        |shrink_to_line_bounds             |
|J               |	将选区中的内容合并为一行	                                    |join_selections                   |
|Alt-J           |  将选区中的内容合并为一行，然后选择合并中新插入的空格	        |join_selections_space             |
|K               |	在多个选区中，只保留符合指定正则表达式的选区	                |keep_selections                   |
|Alt-K           |	在多个选区中，移除符合指定正则表达式的选区	                  |remove_selections                 |
|Ctrl-c          |	注释/解注释所选内容	                                          |toggle_comments                   |
|Alt-o, Alt-up   |	扩展选择到语法树的父节点 (TS)	                                |expand_selection                  |
|Alt-i, Alt-down |  缩小语法树对象选区 (TS)	                                      |shrink_selection                  |
|Alt-p, Alt-left |  选择语法树中的前一个兄弟节点 (TS)	                            |select_prev_sibling               |
|Alt-n, Alt-right|	选择语法树中的后一个兄弟节点 (TS)	                            |select_next_sibling               |

### 搜索

|快捷键|	描述	                    |命令            |
|---   |----------------------------|----------------|
|/     |	按正则表达式搜索	        |search          |
|?     |	按正则表达式向前搜索	    |rsearch         |
|n     |	搜索下一个	              |search_next     |
|N     |	搜索上一个	              |search_prev     |
|*     |	使用当前选择作为搜索模式	|search_selection|

## 子模式

|Key   |	Description	                |Command     |
|------|------------------------------|------------|
|v     |	Enter select (extend) mode	|select_mode |
|g     |	Enter goto mode	            |N/A         |
|m     |	Enter match mode	          |N/A         |
|:     |	Enter command mode	        |command_mode|
|z     |	Enter view mode	            |N/A         |
|Z     |	Enter sticky view mode	    |N/A         |
|Ctrl-w|	Enter window mode	          |N/A         |
|Space |	Enter space mode	          |N/A         |

## 视图模式

|Key             |	Description	                                              |Command              |
|----------------|------------------------------------------------------------|---------------------|
|z, c            |	Vertically center the line	                              |align_view_center    |
|t               |	Align the line to the top of the screen	                  |align_view_top       |
|b               |	Align the line to the bottom of the screen	              |align_view_bottom    |
|m               |	Align the line to the middle of the screen (horizontally)	|align_view_middle    |
|j, down         |	Scroll the view downwards	                                |scroll_down          |
|k, up           |	Scroll the view upwards	                                  |scroll_up            |
|Ctrl-f, PageDown|	Move page down	                                          |page_down            |
|Ctrl-b, PageUp  |	Move page up	                                            |page_up              |
|Ctrl-u          |	Move cursor and page half page up	                        |page_cursor_half_up  |
|Ctrl-d          |	Move cursor and page half page down	                      |page_cursor_half_down|

### GoTo模式

|Key|	Description	                                                                    |Command                 |
|---|---------------------------------------------------------------------------------|------------------------|
|g  |	Go to line number <n> else start of file	                                      |goto_file_start         |
|e  |	Go to the end of the file	                                                      |goto_last_line          |
|f  |	Go to files in the selections	                                                  |goto_file               |
|h  |	Go to the start of the line	                                                    |goto_line_start         |
|l  |	Go to the end of the line	                                                      |goto_line_end           |
|s  |	Go to first non-whitespace character of the line	                              |goto_first_nonwhitespace|
|t  |	Go to the top of the screen	                                                    |goto_window_top         |
|c  |	Go to the middle of the screen	                                                |goto_window_center      |
|b  |	Go to the bottom of the screen	                                                |goto_window_bottom      |
|d  |	Go to definition (LSP)	                                                        |goto_definition         |
|y  |	Go to type definition (LSP)	                                                    |goto_type_definition    |
|r  |	Go to references (LSP)	                                                        |goto_reference          |
|i  |	Go to implementation (LSP)	                                                    |goto_implementation     |
|a  |	Go to the last accessed/alternate file	                                        |goto_last_accessed_file |
|m  |	Go to the last modified/alternate file	                                        |goto_last_modified_file |
|n  |	Go to next buffer	                                                              |goto_next_buffer        |
|p  |	Go to previous buffer	                                                          |goto_previous_buffer    |
|.  |	Go to last modification in current file	                                        |goto_last_modification  |
|j  |	Move down textual (instead of visual) line	                                    |move_line_down          |
|k  |	Move up textual (instead of visual) line	                                      |move_line_up            |
|w  |	Show labels at each word and select the word that belongs to the entered labels	|goto_word               |

### 匹配模式

|Key         |	Description	                                 |Command                 |
|------------|-----------------------------------------------|------------------------|
|m           |	Goto matching bracket (TS)	                 |match_brackets          |
|s \<char>    |	Surround current selection with <char>	     |surround_add            |
|r \<from><to>|	Replace surround character <from> with <to>	 |surround_replace        |
|d \<char>    |	Delete surround character <char>	           |surround_delete         |
|a \<object>  |	Select around textobject	                   |select_textobject_around|
|i \<object>  |	Select inside textobject	                   |select_textobject_inner |

### 窗口模式

|Key             |	Description	                                          |Command        |
|----------------|--------------------------------------------------------|---------------|
|w, Ctrl-w       |	Switch to next window	                                |rotate_view    |
|v, Ctrl-v       |	Vertical right split	                                |vsplit         |
|s, Ctrl-s       |	Horizontal bottom split	                              |hsplit         |
|f	Go           | to files in the selections in horizontal splits	      |goto_file      |
|F	Go           | to files in the selections in vertical splits	        |goto_file      |
|h, Ctrl-h, Left |	Move to left split	                                  |jump_view_left |
|j, Ctrl-j, Down |	Move to split below	                                  |jump_view_down |
|k, Ctrl-k, Up   |	Move to split above	                                  |jump_view_up   |
|l, Ctrl-l, Right|	Move to right split	                                  |jump_view_right|
|q, Ctrl-q       |	Close current window	                                |wclose         |
|o, Ctrl-o       |	Only keep the current window, closing all the others	|wonly          |
|H	             |Swap window to the left	                                |swap_view_left |
|J	             |Swap window downwards	                                  |swap_view_down |
|K	             |Swap window upwards	                                    |swap_view_up   |
|L	             |Swap window to the right	                              |swap_view_right|

### Space模式

|Key    |	Description	                                              |Command                                 |
|-----  |-----------------------------------------------------------|----------------------------------------|
|f      |	Open file picker	                                        |file_picker                             |
|F      |	Open file picker at current working directory	            |file_picker_in_current_directory        |
|b      |	Open buffer picker	                                      |buffer_picker                           |
|j      |	Open jumplist picker	                                    |jumplist_picker                         |
|g      |	Debug (experimental)	                                    |N/A                                     |
|k      |	Show documentation for item under cursor in a popup (LSP)	|hover                                   |
|s      |	Open document symbol picker (LSP)	                        |symbol_picker                           |
|S      |	Open workspace symbol picker (LSP)	                      |workspace_symbol_picker                 |
|d      |	Open document diagnostics picker (LSP)	                  |diagnostics_picker                      |
|D      |	Open workspace diagnostics picker (LSP)	                  |workspace_diagnostics_picker            |
|r      |	Rename symbol (LSP)	                                      |rename_symbol                           |
|a      |	Apply code action (LSP)	                                  |code_action                             |
|h      |	Select symbol references (LSP)	                          |select_references_to_symbol_under_cursor|
|'      |	Open last fuzzy picker	                                  |last_picker                             |
|w      |	Enter window mode	                                        |N/A                                     |
|c      |	Comment/uncomment selections	                            |toggle_comments                         |
|C      |	Block comment/uncomment selections	                      |toggle_block_comments                   |
|Alt-c  |	Line comment/uncomment selections	                        |toggle_line_comments                    |
|p      |	Paste system clipboard after selections	                  |paste_clipboard_after                   |
|P      |	Paste system clipboard before selections	                |paste_clipboard_before                  |
|y      |	Yank selections to clipboard	                            |yank_to_clipboard                       |
|Y      |	Yank main selection to clipboard	                        |yank_main_selection_to_clipboard        |
|R      |	Replace selections by clipboard contents	                |replace_selections_with_clipboard       |
|/      |	Global search in workspace folder	                        |global_search                           |
|?      |	Open command palette	                                    |command_palette                         |

### Popup

|Key   |	Description |
|------|--------------|
|Ctrl-u|	Scroll up   |
|Ctrl-d|	Scroll down |

### Unimpaired

|Key   |	Description	                               |Command            |
|------|---------------------------------------------|-------------------|
|]d    |	Go to next diagnostic (LSP)	               |goto_next_diag     |
|[d    |	Go to previous diagnostic (LSP)	           |goto_prev_diag     |
|]D    |	Go to last diagnostic in document (LSP)	   |goto_last_diag     |
|[D    |	Go to first diagnostic in document (LSP)	 |goto_first_diag    |
|]f    |	Go to next function (TS)	                 |goto_next_function |
|[f    |	Go to previous function (TS)	             |goto_prev_function |
|]t    |	Go to next type definition (TS)	           |goto_next_class    |
|[t    |	Go to previous type definition (TS)	       |goto_prev_class    |
|]a    |	Go to next argument/parameter (TS)	       |goto_next_parameter|
|[a    |	Go to previous argument/parameter (TS)	   |goto_prev_parameter|
|]c    |	Go to next comment (TS)	                   |goto_next_comment  |
|[c    |	Go to previous comment (TS)	               |goto_prev_comment  |
|]T    |	Go to next test (TS)	                     |goto_next_test     |
|[T    |	Go to previous test (TS)	                 |goto_prev_test     |
|]p    |	Go to next paragraph	                     |goto_next_paragraph|
|[p    |	Go to previous paragraph	                 |goto_prev_paragraph|
|]g    |	Go to next change	                         |goto_next_change   |
|[g    |	Go to previous change	                     |goto_prev_change   |
|]G    |	Go to last change	                         |goto_last_change   |
|[G    |	Go to first change	                       |goto_first_change  |
|]Space|	Add newline below	                         |add_newline_below  |
|[Space|	Add newline above	                         |add_newline_above  |


### 插入模式

|Key                               |	Description	               |Command               |
|----------------------------------|-----------------------------|----------------------|
|Escape                            |	Switch to normal mode	     |normal_mode           |
|Ctrl-s                            |	Commit undo checkpoint	   |commit_undo_checkpoint|
|Ctrl-x                            |	Autocomplete	             |completion            |
|Ctrl-r                            |	Insert a register content	 |insert_register       |
|Ctrl-w, Alt-Backspace             |	Delete previous word	     |delete_word_backward  |
|Alt-d, Alt-Delete                 |	Delete next word	         |delete_word_forward   |
|Ctrl-u                            |	Delete to start of line	   |kill_to_line_start    |
|Ctrl-k                            |	Delete to end of line	     |kill_to_line_end      |
|Ctrl-h, Backspace, Shift-Backspace|	Delete previous char	     |delete_char_backward  |
|Ctrl-d, Delete                    |	Delete next char	         |delete_char_forward   |
|Ctrl-j, Enter                     |	Insert new line	           |insert_newline        |

These keys are not recommended, but are included for new users less familiar with modal editors.

|Key     |	Description	           |Command              |
|--------|-------------------------|---------------------|
|Up      |	Move to previous line	 |move_line_up         |
|Down    |	Move to next line	     |move_line_down       |
|Left    |	Backward a char	       |move_char_left       |
|Right   |	Forward a char	       |move_char_right      |
|PageUp  |	Move one page up	     |page_up              |
|PageDown|	Move one page down	   |page_down            |
|Home    |	Move to line start	   |goto_line_start      |
|End     |	Move to line end	     |goto_line_end_newline|

### Picker

|Key                  |	Description                                               |
|---------------------|-----------------------------------------------------------|
|Shift-Tab, Up, Ctrl-p|	Previous entry                                            |
|Tab, Down, Ctrl-n    |	Next entry                                                |
|PageUp, Ctrl-u       |	Page up                                                   |
|PageDown, Ctrl-d     |	Page down                                                 |
|Home                 |	Go to first entry                                         |
|End                  |	Go to last entry                                          |
|Enter                |	Open selected                                             |
|Alt-Enter            |	Open selected in the background without closing the picker|
|Ctrl-s               |	Open horizontally                                         |
|Ctrl-v               |	Open vertically                                           |
|Ctrl-t               |	Toggle preview                                            |
|Escape, Ctrl-c       |	Close picker                                              |

### Prompt

|Key                                  |	Description                                                           |
|-------------------------------------|-----------------------------------------------------------------------|
|Escape, Ctrl-c                       |	Close prompt                                                          |
|Alt-b, Ctrl-Left                     |	Backward a word                                                       |
|Ctrl-b, Left                         |	Backward a char                                                       |
|Alt-f, Ctrl-Right                    |	Forward a word                                                        |
|Ctrl-f, Right                        |	Forward a char                                                        |
|Ctrl-e, End                          |	Move prompt end                                                       |
|Ctrl-a, Home                         |	Move prompt start                                                     |
|Ctrl-w, Alt-Backspace, Ctrl-Backspace|	Delete previous word                                                  |
|Alt-d,  Alt-Delete, Ctrl-Delete      |	Delete next word                                                      |
|Ctrl-u                               |	Delete to start of line                                               |
|Ctrl-k                               |	Delete to end of line                                                 |
|Backspace, Ctrl-h, Shift-Backspace   |	Delete previous char                          |
|Delete, Ctrl-d                       |	Delete next char                                                      |
|Ctrl-s                               |	Insert a word under doc cursor, may be changed to Ctrl-r Ctrl-w later |
|Ctrl-p, Up                           |	Select previous history                                               |
|Ctrl-n, Down                         |	Select next history                                                   |
|Ctrl-r                               |	Insert the content of the register selected by following input char   |
|Tab                                  |	Select next completion item                                           |
|BackTab                              |	Select previous completion item                                       |
|Enter                                |	Open selected                                                         |
