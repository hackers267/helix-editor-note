# 结合使用 _helix_ 和 _nushell_

我们知道 _helix_ 是一个后现代的终端编辑器，而 _nushell_ 是一个现代版本的 _shell_。那么我们如何在 _helix_ 中发挥 _nushell_ 的优势呢？

## 设置 _helix_ 的 _shell_

如果我们需要在 _helix_ 中充分利用 _nushell_ 的能力，我们首先要做的是把 _helix_ 的配置文件 _config.tom_ 的 `[editor]`字段中的 `shell` 选项如下设置：

```toml
[editor]
shell = ["nu", "-c"]
```

## 编写 _nushell_ 脚本

当我们设置好 _helix_ 后，还需要在 _nushell_ 中编写我们需要的脚本。如编写一个 `day.nu` 脚本：

```nushell
#!/usr/bin/env nu
def day [day: number = 0] {
  let diff = $"($day)day";
  let day = ^date -d $diff +"%x %A";
  $day | str trim -c '"'
}

def main [n: number = 0] {
  day $n
}

```

在编写完我们的脚本后，把它放在我们配置的 `PATH` 中。之后，我们就可以使用命令来利用我们的脚本了。

## 脚本使用方法

快捷键：

- `!`: *insert_output* 在光标前插入脚本的执行结果
- `|`: *pipe* 把光标下的内容作为脚本的输入，然后用脚本的输出替换选择的内容
- `Alt+!`: *append_output* 在光标后插入脚本的执行结果
- `Alt+|`: *pipe-to* 把选择的内容作为脚本的输入执行

*sh* 在命令模式下执行我们的脚本，并把输出以弹框的形式显示在 _helix_ 中。
