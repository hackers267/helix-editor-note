# Summary

- [使用](./usage.md)
- [跳转](./jump.md)
- [寄存器](./register.ms)
- [快捷键](./keymaps.md)
- [命令详解](./command.md)
- [nushell](./nushell.md)
- [编辑Markdow](./markdown.md)
- [Chapter 1](./chapter_1.md)
