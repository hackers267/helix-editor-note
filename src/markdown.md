# 使用 *helix* 编辑 markdown

我们知道 *Obsidian* 和 *Typora* 是编辑markdown的两大编辑器之一。但在网上看到了一个题为 *为什么我不使用Obsidian* 的视频，其作者是通过使用 *helix*来达到编辑
markdown的目的的。再结合其另外一个使用 *slides*的视频，在一定的程序上，我们就可以通过 *helix* 和 *slides* 来达到部分替代 *Obsidian* 和 *Typora*的目的。

## 介绍

*helix* 在这里就不多作介绍了，看到这里的朋友，相关多多少少都了解 *helix*, 而 *slides* 是一个使用 *go* 语言开发的终端markdown预览工具。可以直接使用 `slides 文件` 来实时预览 markdown 文件。

## helix 对 markdown 的支持

目前 *helix* 默认支持markdown的语法高亮。如果需要支持 markdown 的语法，那么就需要安装 *marksman*。

首先，我们可以通过命令 `hx --health markdown` 来查看 *helix* 对 markdown 的支持，一般的输出如下：

```text
Configured language servers:
  ✔️ marksman: /usr/bin/marksman
Configured debug adapter: None
Highlight queries: ✔️
Textobject queries: ❌
Indent queries: ❌
```

如果你的输出和上面的类似，那么，就可以直接使用 *helix* 来编辑 markdown文件了。

在编辑 *markdown* 的时候，我们可以利用 *helix* 的一系列功能。比如我们可以使用 *helix* 的 pipe功能来对内容进行排序。
